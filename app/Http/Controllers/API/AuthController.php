<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\User;

class AuthController extends Controller
{

    public function show(){
        return User::all();
    }

    public function delete($id){
        $user = User::find($id)->delete();
        return 'ok';
    }

    public function edit($id){
        $user = User::find($id);
        return response()->json($user);
    }

    // public function delete(Request $request)
    // {
    //     $id = $request->input('id');
    //     $item = new User;
    //     $result = $item->deleteById($id);
    //     return response()->json(['success' => $result]);
    // }

    public function update($id, Request $req){
        
        $user = User::find($id);
        $user->name = $req['name'];
        $user->email = $req['email'];
        $user->gender = $req['gender'];
        $user->country = $req['country'];
        $user->state = $req['state'];
        $user->password = $req['password'];
        $user->c_password = $req['c_password'];
        $user->address = $req['address'];
        $user->contact = $req['contact'];
        $user->dob = $req['dob'];
        // $filename = time() ."DKB.".$req->file('file')->getClientOriginalExtension();
        // echo $req->file('file')->storeAs('uploads', $filename);
        // $customer->Profile = $filename;
       
        $user->save();
        $response = [
            'success' => true,
            // 'data' => $success,
            'message' => 'User register successfully'
        ];

        return response()->json($response, 200);
    }
    public function register(Request $req){
        $validator = Validator::make($req->all(),[
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if($validator->fails()){
            $response = [
                'success' => false,
                'message' => $validator->errors()
            ];
            return response()->json($response, 400);
        }
        $input = $req->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);

        $success['token'] = $user->createToken('MyApp')->plainTextToken;
        $success['name'] = $user->name;

        $response = [
            'success' => true,
            'data' => $success,
            'message' => 'User register successfully'
        ];

        return response()->json($response, 200);
    }

    public function login(Request $req){
        if (Auth::attempt(['email'=>$req->email,'password'=>$req->password])) {
            $user = Auth::user();

            $success['token'] = $user->createToken('MyApp')->plainTextToken;
            $success['name'] = $user->name;
    
            $response = [
                'success' => true,
                'data' => $success,
                'message' => 'User login successfully'
            ];
            return response()->json($response, 200);
        }else{
            $response = [
                'success' => false,
                'message' => 'Unauthorised User'
            ];
            return response()->json($response);
        }
    }
}
