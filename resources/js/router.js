import { createWebHistory, createRouter } from "vue-router";

import login  from './pages/login.vue';
import register from './pages/register.vue';
import home from './pages/home.vue';
import store from './store/index.js';

const routes = [
    {
        path : '/',
        name : 'Home',
        component : home,
        meta:{
            requiresAuth:true
        }
    },
    {
        path : '/register',
        name : 'Register',
        component : register,
        meta:{
            requiresAuth:false
        }
    },
    
    {
        path : '/login',
        name : 'Login',
        component : login,
        meta:{
            requiresAuth:false
        }
    },
    
];

const router = createRouter({
    history : createWebHistory(),
    routes
});

router.beforeEach((to,from) =>{
    if(to.meta.requiresAuth && store.getters.getToken == 0){
         return { name : 'Login' }
    }
    if(to.meta.requiresAuth == false && store.getters.getToken != 0){
        return { name : 'Home' }
    }
})

export default router;
